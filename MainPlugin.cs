﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Terraria;
using TerrariaApi.Server;
using TShockAPI;

namespace KekDevelops.tSPlugins.InventorySee
{
    [ApiVersion(2, 1)]
    public class MainPlugin : TerrariaPlugin
    {
        public override string Author => "Zoom L1 | AgaSpace";
        public override string Name => "InventorySee"; // No SSC
        public MainPlugin(Main game) : base(game) { }

        public override void Initialize()
        {
            Commands.ChatCommands.Add(new Command("kd.invsee", InvSeeCmd, new string[] { "invsee", "inventorysee" }));
        }

        private static void InvSeeCmd(CommandArgs args)
        {
            PlayerInfo info = args.Player.GetPlayerInfo();
            if (args.Parameters.Count == 0)
            {
                if (info.Copies != false)
                {
                    info.Backup.UploadInventory(args.Player);
                    args.Player.SendErrorMessage("Inventory has been restored.");
					info.Backup = new VisualPlayer(new Player());
                    info.Copies = false;
                    return;
                }
                args.Player.SendErrorMessage("You are currently not seeing anyone's inventory.");
                args.Player.SendErrorMessage("Use '" + Commands.Specifier + "invsee <player name>' to begin.");
                return;
            }
            List<TSPlayer> players = TSPlayer.FindByNameOrID(args.Parameters[0]);
			
            if (players.Count == 0)
            {
                args.Player.SendErrorMessage("Invalid player!");
                return;
            }
            if (players.Count > 1)
            {
                args.Player.SendMultipleMatchError(players.Select(t => t.Name));
                return;
            }
            try
            {
                if (info.Copies == false)
                {
                    info.Backup = VisualPlayer.GetPlayer(args.Player);
                    info.Copies = true;
                }
                players[0].UploadInventory(args.Player);
                args.Player.SendSuccessMessage("Copied " + players[0].Name + "'s inventory.");
            }
            catch (Exception ex)
            {
                if (info.Copies != false)
                {
                    info.Backup.UploadInventory(args.Player);
                    TShock.Log.ConsoleError(ex.ToString());
                    args.Player.SendErrorMessage("Something went wrong... restored your inventory.");
                }
            }
        }
    }

    public struct VisualPlayer // Параметры игрока, которые показывают его самого
    {
        public VisualPlayer(Player plr)
        {
            armor = plr.armor;
            dye = plr.dye;
            miscEquips = plr.miscEquips;
            miscDyes = plr.miscDyes;
            inventory = plr.inventory;

            skinVariant = plr.skinVariant;
            hair = plr.hair;

            hairDye = plr.hairDye;
            difficulty = plr.difficulty;

            hideVisibleAccessory = plr.hideVisibleAccessory;
            extraAccessory = plr.extraAccessory;

            hideMisc = plr.hideMisc;

            hairColor = plr.hairColor;
            skinColor = plr.skinColor;
            eyeColor = plr.eyeColor;
            shirtColor = plr.shirtColor;
            underShirtColor = plr.underShirtColor;
            pantsColor = plr.pantsColor;
            shoeColor = plr.shoeColor;

            statLife = plr.statLife;
            statLifeMax = plr.statLifeMax;
            statMana = plr.statMana;
            statManaMax = plr.statManaMax;
        }

        public Item[] armor;
        public Item[] dye;
        public Item[] miscEquips;
        public Item[] miscDyes;
        public Item[] inventory;

        public int skinVariant;
        public int hair;

        public byte hairDye;
        public byte difficulty;

        public bool[] hideVisibleAccessory;
        public bool extraAccessory;

        public BitsByte hideMisc;

        public Color hairColor;
        public Color skinColor;
        public Color eyeColor;
        public Color shirtColor;
        public Color underShirtColor;
        public Color pantsColor;
        public Color shoeColor;

        public int statLife;
        public int statLifeMax;
        public int statMana;
        public int statManaMax;

        public static VisualPlayer GetPlayer(TSPlayer plr) => GetPlayer(plr.TPlayer);

        public static VisualPlayer GetPlayer(Player plr) => new VisualPlayer(plr);

        public void UploadInventory(TSPlayer t)
        {
            Main.ServerSideCharacter = true;
            t.SendData(PacketTypes.WorldInfo, "", 0, 0f, 0f, 0f, 0);

            t.TPlayer.skinVariant = this.skinVariant;
            t.TPlayer.hair = this.hair;
            t.TPlayer.hairDye = this.hairDye;
            t.TPlayer.hideVisibleAccessory = this.hideVisibleAccessory;
            t.TPlayer.hideMisc = this.hideMisc;
            t.TPlayer.hairColor = this.hairColor;
            t.TPlayer.skinColor = this.skinColor;
            t.TPlayer.eyeColor = this.eyeColor;
            t.TPlayer.shirtColor = this.shirtColor;
            t.TPlayer.underShirtColor = this.underShirtColor;
            t.TPlayer.pantsColor = this.pantsColor;
            t.TPlayer.shoeColor = this.shoeColor;
            t.TPlayer.extraAccessory = this.extraAccessory;
            t.TPlayer.difficulty = this.difficulty;
            NetMessage.SendData(4, -1, -1, null, t.Index, 0f, 0f, 0f, 0, 0, 0);

            t.TPlayer.inventory = this.inventory;
            t.TPlayer.armor = this.armor;
            t.TPlayer.dye = this.dye;
            t.TPlayer.miscEquips = this.miscEquips;
            t.TPlayer.miscDyes = this.miscDyes;
            for (int i = 0; i < 259; i++)
            {
                Item item = t.GetItem(i);
                NetMessage.SendData(5, -1, -1, null, t.Index, (float)i, (float)item.prefix, 0f, 0, 0, 0);
            }

            t.TPlayer.statLife = this.statLife;
            t.TPlayer.statLifeMax = this.statLifeMax;
            t.TPlayer.statMana = this.statMana;
            t.TPlayer.statManaMax = this.statManaMax;
            NetMessage.SendData(16, -1, -1, null, t.Index, t.TPlayer.statLife, t.TPlayer.statLifeMax);
            NetMessage.SendData(42, -1, -1, null, t.Index, t.TPlayer.statMana, t.TPlayer.statManaMax);

            Main.ServerSideCharacter = false;
            t.SendData(PacketTypes.WorldInfo, "", 0, 0f, 0f, 0f, 0);
        }
    }

    public static class Extensions
    {
        public static void UploadInventory(this TSPlayer f, TSPlayer t) => VisualPlayer.GetPlayer(f).UploadInventory(t);

        public static Item GetItem(this TSPlayer t, int i)
		{
			return ((float)i > (float)(58 + t.TPlayer.armor.Length + t.TPlayer.dye.Length + t.TPlayer.miscEquips.Length + t.TPlayer.miscDyes.Length + t.TPlayer.bank.item.Length + t.TPlayer.bank2.item.Length + t.TPlayer.bank3.item.Length + 1)) ? t.TPlayer.bank4.item[i - 58 - (t.TPlayer.armor.Length + t.TPlayer.dye.Length + t.TPlayer.miscEquips.Length + t.TPlayer.miscDyes.Length + t.TPlayer.bank.item.Length + t.TPlayer.bank2.item.Length + t.TPlayer.bank3.item.Length + 1) - 1] : (((float)i > (float)(58 + t.TPlayer.armor.Length + t.TPlayer.dye.Length + t.TPlayer.miscEquips.Length + t.TPlayer.miscDyes.Length + t.TPlayer.bank.item.Length + t.TPlayer.bank2.item.Length + 1)) ? t.TPlayer.bank3.item[i - 58 - (t.TPlayer.armor.Length + t.TPlayer.dye.Length + t.TPlayer.miscEquips.Length + t.TPlayer.miscDyes.Length + t.TPlayer.bank.item.Length + t.TPlayer.bank2.item.Length + 1) - 1] : (((float)i > (float)(58 + t.TPlayer.armor.Length + t.TPlayer.dye.Length + t.TPlayer.miscEquips.Length + t.TPlayer.miscDyes.Length + t.TPlayer.bank.item.Length + t.TPlayer.bank2.item.Length)) ? t.TPlayer.trashItem : (((float)i > (float)(58 + t.TPlayer.armor.Length + t.TPlayer.dye.Length + t.TPlayer.miscEquips.Length + t.TPlayer.miscDyes.Length + t.TPlayer.bank.item.Length)) ? t.TPlayer.bank2.item[i - 58 - (t.TPlayer.armor.Length + t.TPlayer.dye.Length + t.TPlayer.miscEquips.Length + t.TPlayer.miscDyes.Length + t.TPlayer.bank.item.Length) - 1] : (((float)i > (float)(58 + t.TPlayer.armor.Length + t.TPlayer.dye.Length + t.TPlayer.miscEquips.Length + t.TPlayer.miscDyes.Length)) ? t.TPlayer.bank.item[i - 58 - (t.TPlayer.armor.Length + t.TPlayer.dye.Length + t.TPlayer.miscEquips.Length + t.TPlayer.miscDyes.Length) - 1] : (((float)i > (float)(58 + t.TPlayer.armor.Length + t.TPlayer.dye.Length + t.TPlayer.miscEquips.Length)) ? t.TPlayer.miscDyes[i - 58 - (t.TPlayer.armor.Length + t.TPlayer.dye.Length + t.TPlayer.miscEquips.Length) - 1] : (((float)i > (float)(58 + t.TPlayer.armor.Length + t.TPlayer.dye.Length)) ? t.TPlayer.miscEquips[i - 58 - (t.TPlayer.armor.Length + t.TPlayer.dye.Length) - 1] : (((float)i > (float)(58 + t.TPlayer.armor.Length)) ? t.TPlayer.dye[i - 58 - t.TPlayer.armor.Length - 1] : (((float)i <= 58f) ? t.TPlayer.inventory[i] : t.TPlayer.armor[i - 58 - 1]))))))));
		}

        public static PlayerInfo GetPlayerInfo(this TSPlayer player)
        {
            if (!player.ContainsData(PlayerInfo.KEY))
            {
                player.SetData<PlayerInfo>(PlayerInfo.KEY, new PlayerInfo());
            }
            return player.GetData<PlayerInfo>(PlayerInfo.KEY);
        }
    }
	
	public class PlayerInfo
    {
        public VisualPlayer Backup { get; set; }
        public bool Copies { get; set; }

        public PlayerInfo()
        {
            Backup = new VisualPlayer(new Player());
            Copies = false;
        }

        public const string KEY = "InventorySee_Data";
    }
}